import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import * as AuthService from "./services/auth.service";
import EventBus from "./common/EventBus";
import Login from "./components/Login";
import React from "react";
import Register from "./components/Register";
import TicketCreator from "./components/TicketCreator";
import TicketList from "./components/TicketList";
import UserList from "./components/UserList";
import { Switch, Route, Link } from "react-router-dom";
import { useEffect } from "react";

const App: React.FC = () => {
  useEffect(() => {
    EventBus.on("logout", logOut);

    return () => {
      EventBus.remove("logout", logOut);
    };
  }, []);

  const logOut = () => {
    AuthService.logout();
  };

  // User actions
  let actions;
  let routes = <Switch>
                  <Route exact path="/" component={Login} />
                </Switch>;

  // Admin
  if(AuthService.getCurrentUser() && AuthService.getCurrentUser().role.name === 'admin'){
    actions = <div className="navbar-nav">
                  <li className="nav-item">
                    <Link to={"/register"} className="nav-link">
                      Registrar usuario
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to={"/tickets"} className="nav-link">
                      Tickets
                    </Link>
                  </li>
                  <li className="nav-item">
                      <Link to={"/users"} className="nav-link">
                        Usuarios
                      </Link>
                  </li>
                  <li className="nav-item">
                    <a href="/" className="nav-link" onClick={logOut}>
                      Salir
                    </a>
                  </li>
              </div>;
    routes =  <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path={["/new-ticket", "/new-ticket"]} component={TicketCreator} />
                <Route exact path={["/tickets", "/tickets"]} component={TicketList} />
                <Route exact path={["/users", "/users"]} component={UserList} />
              </Switch>
  }

  // dev
  if(AuthService.getCurrentUser() && AuthService.getCurrentUser().role.name === 'dev'){
    actions = <div className="navbar-nav">
                  <li className="nav-item">
                    <Link to={"/tickets"} className="nav-link">
                      Tickets
                    </Link>
                  </li>
                  <li className="nav-item">
                    <a href="/" className="nav-link" onClick={logOut}>
                      Salir
                    </a>
                  </li>
              </div>;
    routes =  <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path={["/tickets", "/tickets"]} component={TicketList} />
              </Switch>                
  }

  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <Link to={"/"} className="navbar-brand">
          Keiron
        </Link>
        {actions}
      </nav>

      <div className="container mt-3">
        {routes}
      </div>
    </div>
  );
};

export default App;
