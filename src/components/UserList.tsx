import BootstrapTable from 'react-bootstrap-table-next';
import React, { useState, useEffect } from "react";
import { Button } from 'react-bootstrap';
import { getUsers, deleteUserById } from "../services/user.service";

const UserList: React.FC = () => {
  const [users, setUsers] = useState([]);
  const [currentUserId, setCurrentUserId] = useState(null);

  const deleteUser = async () => {
    // eslint-disable-next-line no-restricted-globals
    if(currentUserId && confirm("Desea borrar el usuario con id: " + currentUserId)){
      await deleteUserById(currentUserId);
      loadUsers();
    }
  }

  let columns = [{
    dataField: 'id',
    text: 'Id',
    headerStyle: { width: '4%' }
  }, {
    dataField: 'name',
    text: 'Nombre'
  }, {
    dataField: 'username',
    text: 'Nombre de usuario'
  }, {
    dataField: 'email',
    text: 'Correo electrónico'
  }, {
    dataField: 'role.name',
    text: 'Role'
  }, {
    dataField: 'actions',
    text: 'Acciones',
    formatter: (rowContent, row) => {
      return (
        <div >
          <Button variant='danger' onClick={deleteUser}>Eliminar</Button>
        </div>
      )
    },
    headerStyle: { width: '20%' },
    events: {
      onMouseEnter: (e, column, columnIndex, row, rowIndex) => { setCurrentUserId(row.id) },
    }
  }];

  const loadUsers = () => {
    getUsers().then(
      (response) => {
        setUsers(response.data);
      },
      (error) => {
        const _users =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

          setUsers(_users);
      }
    );
  }

  useEffect(() => {
    loadUsers()
  }, []);

  return (
    <div className="container">
      <header className="jumbotron">
        <h2>Usuarios</h2>
        <Button href="register" variant='success'>Crear usuario</Button>
        <BootstrapTable keyField='id' data={ users } columns={ columns } />
      </header>
    </div>
  );
};

export default UserList;
