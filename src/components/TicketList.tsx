import BootstrapTable from 'react-bootstrap-table-next';
import React, { useState, useEffect } from "react";
import { Dropdown, Button } from 'react-bootstrap';
import { format } from 'fecha';
import { getStatuses } from "../services/status.service";
import { getTickets, updateTicketAssign, updateTicketStatus, deleteTicketById } from "../services/ticket.service";
import { getUsers } from "../services/user.service";
import * as AuthService from "../services/auth.service";

enum Statuses{
  "toResolve" = "Sin resolver",
  "resolved" = "Resuelto",
  "refused" = "Rechazado",
  "canceled" = "Cancelado"
}

const TicketList: React.FC = () => {
  const [tickets, setTickets] = useState([]);
  const [statuses, setStatuses] = useState([]);
  const [users, setUsers] = useState([]);
  const [currentTicketId, setCurrentTicketId] = useState(null);
  const [currentUser, setCurrentUser] = useState({})

  const assignOnSelect = (assignId) => {
    if(currentTicketId){
      updateTicketAssign(currentTicketId, assignId).then(() => loadTickets())
    }
  }

  const statusOnSelect = (statusId) => {
    if(currentTicketId){
      updateTicketStatus(currentTicketId, statusId).then(() => loadTickets())
    }
  }

  const deleteTicket = async () => {
    // eslint-disable-next-line no-restricted-globals
    if(currentTicketId && confirm("Desea borrar el ticket con id: " + currentTicketId)){
      await deleteTicketById(currentTicketId);
      loadTickets();
    }
  }

  let columns = [{
    dataField: 'id',
    text: 'Id',
    headerStyle: { width: '4%' }
  }, {
    dataField: 'createdBy.name',
    text: 'Creado por'
  }, {
    dataField: 'assignedTo.name',
    text: 'Asignado a'
  }, {
    dataField: 'status.name',
    text: 'Status del ticket',
    formatter: (rowContent) => {
      return (
        <div>
          <p>{Statuses[rowContent]}</p>
        </div>
      )
    }
  }, {
    dataField: 'createdAt',
    text: 'Fecha creación',
    formatter: (rowContent) => {
      if(rowContent){
        const date = format(new Date(rowContent), 'DD-MM-YYYY hh:mm');
        return (
          <div>
            <p>{date}</p>
          </div>
        )
      }
    },
  }, {
    dataField: 'assignedAt',
    text: 'Fecha asignación',
    formatter: (rowContent) => {
      if(rowContent){
        const date = format(new Date(rowContent), 'DD-MM-YYYY hh:mm');
        return (
          <div>
            <p>{date}</p>
          </div>
        )
      }
    },
  }, {
    dataField: 'resolvedAt',
    text: 'Fecha de resolución',
    formatter: (rowContent) => {
      if(rowContent){
        const date = format(new Date(rowContent), 'DD-MM-YYYY hh:mm');
        return (
          <div>
            <p>{date}</p>
          </div>
        )
      }
    },
  }, {
    dataField: 'actions',
    text: 'Acciones',
    formatter: (rowContent, row, rowIndex, extraData) => {
      if(extraData.currentUser.role.name === 'admin'){
        return (
          <div>
            <Dropdown align="end" onSelect={assignOnSelect}>
              <Dropdown.Toggle variant="success" id="dropdown-1">
              Asignar
              </Dropdown.Toggle>
  
              <Dropdown.Menu>
                {extraData.users.map((user) => {
                  return (<Dropdown.Item href="#" key={user.id} eventKey={user.id}>{user.name}</Dropdown.Item>)
                })}
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown align="end" onSelect={statusOnSelect}>
              <Dropdown.Toggle variant="warning" id="dropdown-2">
              Modificar status
              </Dropdown.Toggle>
  
              <Dropdown.Menu>
                {extraData.statuses.map((status) => {
                  return (<Dropdown.Item href="#" key={status.id} eventKey={status.id} >{Statuses[status.name]}</Dropdown.Item>)
                })}
              </Dropdown.Menu>
            </Dropdown>
            <Button variant='danger' onClick={deleteTicket}>Eliminar</Button>
          </div>
        )
      }
      if(extraData.currentUser.role.name === 'dev'){
        return (
          <div>
            <Dropdown align="end" onSelect={statusOnSelect}>
              <Dropdown.Toggle variant="warning" id="dropdown-2">
              Modificar status
              </Dropdown.Toggle>
  
              <Dropdown.Menu>
                {extraData.statuses.map((status) => {
                  if(status.name !== 'toResolve'){
                    return (<Dropdown.Item href="#" key={status.id} eventKey={status.id} >{Statuses[status.name]}</Dropdown.Item>)
                  }

                  return null;
                })}
              </Dropdown.Menu>
            </Dropdown>
          </div>
        )
      }
    },
    formatExtraData: { statuses, users, currentUser },
    headerStyle: { width: '20%' },
    events: {
      onMouseEnter: (e, column, columnIndex, row, rowIndex) => { setCurrentTicketId(row.id) },
    }
  }];

  const loadTickets = () => {
    getTickets().then(
      (response) => {
        setTickets(response.data);
      },
      (error) => {
        const _tickets =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setTickets(_tickets);
      }
    );
  };

  useEffect(() => {
    setCurrentUser(AuthService.getCurrentUser());
    loadTickets();
    getStatuses().then(
      (response) => {
        setStatuses(response.data);
      },
      (error) => {
        const _statutes =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setStatuses(_statutes);
      }
    );
    getUsers().then(
      (response) => {
        setUsers(response.data);
      },
      (error) => {
        const _users =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

          setUsers(_users);
      }
    );
  }, []);

  let createButton;

  if(AuthService.getCurrentUser().role.name === 'admin'){
    createButton = <Button href="new-ticket" variant='success'>Crear ticket</Button>;
  }

  return (
    <div className="container">
      <header className="jumbotron">
        <h2>Tickets</h2>
        {createButton}
        <BootstrapTable keyField='id' data={ tickets } columns={ columns } />
      </header>
    </div>
  );
};

export default TicketList;
