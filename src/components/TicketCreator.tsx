import * as Yup from "yup";
import IStatus from "../types/status.type";
import ITicket from "../types/ticket.type";
import IUser from "../types/user.type";
import React, { useState, useEffect } from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { RouteComponentProps } from "react-router-dom";
import { create } from "../services/ticket.service";
import { getStatuses } from "../services/status.service";
import { getUsers } from "../services/user.service";

interface RouterProps {
  history: string;
}

type Props = RouteComponentProps<RouterProps>;

enum Statuses{
  "toResolve" = "Sin resolver",
  "resolved" = "Resuelto",
  "refused" = "Rechazado",
  "canceled" = "Cancelado"
}

const TicketCreator: React.FC<Props> = ({ history }) => {
  const [message, setMessage] = useState<string>("");
  const [statuses, setStatuses] = useState([]);
  const [successful, setSuccessful] = useState<boolean>(false);
  const [users, setUsers] = useState([]);

  const initialValues: ITicket = {
    assignedTo: 0,
    status: 0, // TODO: default, improve logic
    assignedAt: new Date(),
  };
  
  useEffect(() => {
    getStatuses().then(
      (response) => {
        setStatuses(response.data);
      },
      (error) => {
        const _statutes =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setStatuses(_statutes);
      }
    );
    getUsers().then(
      (response) => {
        setUsers(response.data);
      },
      (error) => {
        const _users =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

          setUsers(_users);
      }
    );
  }, []);


  const validationSchema = Yup.object().shape({
    assignedTo: Yup.string()
      .test(
        "len",
        "Por favor seleccione un valor.",
        (val: any) =>
          val &&
          val > 0
      )
      .required("This field is required!"),
    status: Yup.string()
      .test(
        "len",
        "Por favor seleccione un valor.",
        (val: any) =>
          val &&
          val > 0
      )
      .required("This field is required!"),
  });

  const handleTicketCreator = (formValue: ITicket) => {
    const { assignedTo, status } = formValue;

    create(assignedTo, status).then(
      (response) => {
        setMessage(response.data.message);
        setSuccessful(true);
        history.push("/tickets");
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setMessage(resMessage);
        setSuccessful(false);
      }
    );
  };

  const handleStatus = (val, val2) => console.log(val, val2)

  return (
    <div className="col-md-12">
      <div className="card card-container">
        <h3>Crear Ticket</h3>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleTicketCreator}
        >
          <Form>
            {!successful && (
              <div>
                <div className="form-group">
                  <label htmlFor="assignedTo"> Asignar a </label>
                  <Field name="assignedTo" as="select" className="form-control" onSelect={handleStatus}>
                    <option value="0">Seleccione</option>
                    {users.map((user:IUser) => {
                      return (<option value={user.id} key={user.id}>{user.name}</option>)
                    })}
                  </Field>
                  <ErrorMessage
                    name="assignedTo"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="status"> Seleccionar status </label>
                  <Field name="status" as="select" className="form-control">
                    <option value="0">Seleccione</option>
                    {statuses.map((status: IStatus) => {
                      return (<option value={status.id} key={status.id}>{ Statuses[status.name] }</option>)
                    })}
                  </Field>
                  <ErrorMessage
                    name="status"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>
                <div className="form-group">
                  <button type="submit" className="btn btn-primary btn-block">Crear ticket</button>
                </div>
              </div>
            )}

            {message && (
              <div className="form-group">
                <div
                  className={
                    successful ? "alert alert-success" : "alert alert-danger"
                  }
                  role="alert"
                >
                  {message}
                </div>
              </div>
            )}
          </Form>
        </Formik>
      </div>
    </div>
  );
};

export default TicketCreator;
