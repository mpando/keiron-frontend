import * as Yup from "yup";
import IStatus from "../types/status.type";
import IUser from "../types/user.type";
import React, { useState, useEffect } from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { RouteComponentProps } from "react-router-dom";
import { getRoles } from "../services/role.service";
import { register } from "../services/auth.service";

interface RouterProps {
  history: string;
}

type Props = RouteComponentProps<RouterProps>;

const Register: React.FC<Props> = ({ history }) => {
  const [successful, setSuccessful] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const [roles, setRoles] = useState([]);

  const initialValues: IUser = {
    name: "",
    username: "",
    email: "",
    role: "",
    password: "",
  };

  useEffect(() => {
    getRoles().then(
      (response) => {
        setRoles(response.data);
      },
      (error) => {
        const _statutes =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setRoles(_statutes);
      }
    );
  }, []);

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .test(
        "len",
        "El nombre debe estar entre los 3 y 20 carácteres",
        (val: any) =>
          val &&
          val.toString().length >= 3 &&
          val.toString().length <= 20
      )
      .required("Este campo es requerido"),
    email: Yup.string()
      .email("No es un email válido")
      .required("Este campo es requerido"),
    password: Yup.string()
      .test(
        "len",
        "El password debe estar entre los 6 y 40 carácteres",
        (val: any) =>
          val &&
          val.toString().length >= 6 &&
          val.toString().length <= 40
      )
      .required("Este campo es requerido"),
    role: Yup.string()
      .test(
        "len",
        "Por favor seleccione un valor",
        (val: any) =>
          val &&
          val > 0
      )
      .required("Este campo es requerido")
  });

  const handleRegister = (formValue: IUser) => {
    const { name, username, email, password, role } = formValue;

    register(name, username, email, password, role).then(
      (response) => {
        setMessage(response.data.message);
        setSuccessful(true);
        history.push("/users");
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setMessage(resMessage);
        setSuccessful(false);
      }
    );
  };

  return (
    <div className="col-md-12">
      <div className="card card-container">
        <img
          src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
          alt="profile-img"
          className="profile-img-card"
        />
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleRegister}
        >
          <Form>
            {!successful && (
              <div>
                <div className="form-group">
                  <label htmlFor="name"> Nombre </label>
                  <Field name="name" type="text" className="form-control" />
                  <ErrorMessage
                    name="name"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="username"> Nombre de usuario </label>
                  <Field name="username" type="text" className="form-control" />
                  <ErrorMessage
                    name="username"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="email"> Email </label>
                  <Field name="email" type="email" className="form-control" />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="role"> Rol </label>
                  <Field name="role" as="select" className="form-control">
                    <option value="0">Seleccione rol</option>
                    {roles.map((status:IStatus) => {
                      return (<option value={status.id} key={status.id}>{status.name}</option>)
                    })}
                  </Field>
                  <ErrorMessage
                    name="role"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password"> Password </label>
                  <Field
                    name="password"
                    type="password"
                    className="form-control"
                  />
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="alert alert-danger"
                  />
                </div>

                <div className="form-group">
                  <button type="submit" className="btn btn-primary btn-block">Registrar</button>
                </div>
              </div>
            )}

            {message && (
              <div className="form-group">
                <div
                  className={
                    successful ? "alert alert-success" : "alert alert-danger"
                  }
                  role="alert"
                >
                  {message}
                </div>
              </div>
            )}
          </Form>
        </Formik>
      </div>
    </div>
  );
};

export default Register;
