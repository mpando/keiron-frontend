export default interface IUser {
  id?: any | null,
  name: string,
  username: string,
  email: string,
  password: string,
  role: string
}