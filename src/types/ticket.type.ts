export default interface ITicket {
  id?: any | null,
  assignedTo: number,
  status: number,
  assignedAt: Date,
}