import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3001/";

export const register = (name: string, username: string, email: string, password: string, role: string) => {
  return axios.post(API_URL + "users", {
    name,
    username,
    email,
    password,
    role,
  }, { headers: authHeader() });
};

export const login = (username: string, password: string) => {
  return axios
    .post(API_URL + "auth", {
      username,
      password,
    })
    .then((response) => {
      if (response.data.access_token) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
};

export const logout = () => {
  localStorage.removeItem("user");
};

export const getCurrentUser = () => {
  const userStr = localStorage.getItem("user");
  if (userStr) return JSON.parse(userStr);

  return null;
};
