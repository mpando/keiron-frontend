import axios from "axios";
import authHeader from "./auth-header";
import * as AuthService from "../services/auth.service";

const API_URL = "http://localhost:3001/";

export const getTickets = () => {
  return axios.get(API_URL + "tickets", { headers: authHeader() });
};

export const updateTicketStatus = (id, status) => {
  let payload;
  if(AuthService.getCurrentUser().role.name === 'admin'){
    payload = { status };
  }
  // Here is a business rule, just Devs could make resolvedAt
  if(AuthService.getCurrentUser().role.name === 'dev'){
    payload = { status, resolvedAt: new Date().toJSON() };
  }
  return axios.patch(API_URL + "tickets/"+ id, payload, { headers: authHeader() });
};

export const updateTicketAssign = (id, assignedTo) => {
  return axios.patch(API_URL + "tickets/"+ id, { assignedTo, assignedAt: new Date().toJSON() }, { headers: authHeader() });
};

export const resolveTicketById = (id) => {
  return axios.patch(API_URL + "tickets/"+ id, { status: 2, resolvedAt: new Date().toJSON() }, { headers: authHeader() });
};

export const create = (assignedTo: number, status: number) => {
  return axios.post(API_URL + "tickets", {
    assignedTo,
    status,
    assignedAt: new Date(),
  }, { headers: authHeader() });
};

export const deleteTicketById = async (ticketId: string,) => {
  return axios.delete(API_URL + "tickets/" + ticketId, { headers: authHeader() });
};
