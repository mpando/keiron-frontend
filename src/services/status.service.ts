import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3001/";

export const getStatuses = () => {
  return axios.get(API_URL + "status", { headers: authHeader() });
};
