export default function authHeader() {
  const userStr = localStorage.getItem("user");
  let user = { access_token: null };

  if (userStr)
    user = JSON.parse(userStr);

  if (user && user.access_token) {
    return { Authorization: 'Bearer ' + user.access_token }; // for Spring Boot back-end
    // return { 'x-access-token': user.access_token };       // for Node.js Express back-end
  } else {
    return {};
  }
}