import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3001/";

export const getUsers = async () => {
  return await axios.get(API_URL + "users", { headers: authHeader() });
};

export const deleteUserById = async (userId: string,) => {
  return axios.delete(API_URL + "users/" + userId, { headers: authHeader() });
};
